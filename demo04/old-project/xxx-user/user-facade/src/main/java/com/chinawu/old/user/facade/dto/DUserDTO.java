package com.chinawu.old.user.facade.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * <p class="detail">
 * 功能：用户模块对外Dubbo用户类
 * </p>
 *
 * @ClassName: DUserDTO
 * @version V1.0
 * @author wuyubin
 * @date 2020年04月23日 13:23
 */
public class DUserDTO implements Serializable{

    private static final long serialVersionUID = 5717044574993112398L;
    /**用户id**/
    private Long userId;
    /**用户姓名**/
    private String userName;
    /**创建时间**/
    private Date createTime;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "DUserDTO{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", createTime=" + createTime +
                '}';
    }
}
