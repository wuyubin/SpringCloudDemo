package com.chinawu.old.user.facade.interfaces;

import com.chinawu.old.user.facade.dto.DUserDTO;

/**
 * <p class="detail">
 * 功能：用户模块对外Dubbo服务类接口定义
 * </p>
 *
 * @ClassName: DUserService
 * @version V1.0
 * @author wuyubin
 * @date 2020年04月23日 13:56
 */
public interface DUserService {
    /**
     * <p >
     * 功能：根据用户id查询用户信息
     * </p>
     * @param
     * @author wuyubin
     * @date  2020年04月23日
     * @return
     */
    DUserDTO getByUserId(Long userId);

}
