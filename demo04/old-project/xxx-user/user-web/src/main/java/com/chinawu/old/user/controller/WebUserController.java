package com.chinawu.old.user.controller;

import com.chinawu.old.user.intf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/web/user")
public class WebUserController {

    @Autowired
    UserService userServiceImpl;

    @RequestMapping(value = "/detail")
    public String getDetail(Long userId){
        return userServiceImpl.getNameById(userId);
    }

    @RequestMapping(value = "/addCart")
    public String addCart(Long userId,Long goodsId){
        return userServiceImpl.addCart(userId,goodsId);
    }

}
