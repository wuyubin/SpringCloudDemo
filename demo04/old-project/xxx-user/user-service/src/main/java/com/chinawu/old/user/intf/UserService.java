package com.chinawu.old.user.intf;

/**
 * <p class="detail">
 * 功能：用户模块内部服务类接口定义
 * </p>
 *
 * @ClassName: UserService
 * @version V1.0
 * @author wuyubin
 * @date 2020年04月23日 13:56
 */
public interface UserService {

    /**
     * <p >
     * 功能：根据用户id获取会员名称
     * </p>
     * @param userId 用户id
     * @author wuyubin
     * @date  2020年04月23日
     * @return
     */
    String getNameById(Long userId);
    /**
     * <p >
     * 功能：添加购物车（仅仅演示SpringCloud和Dubbo集成）
     * </p>
     * @param userId 用户id
     * @param goodsId 物品id
     * @author wuyubin
     * @date  2020年04月23日
     * @return
     */
    String addCart(Long userId,Long goodsId);
}
