package com.chinawu.old.user.service;

import com.chinawu.cloud.cart.interfaces.DCartService;
import com.chinawu.old.user.facade.dto.DUserDTO;
import com.chinawu.old.user.facade.interfaces.DUserService;
import com.chinawu.old.user.intf.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("userServiceImpl")
public class UserServiceImpl implements UserService,DUserService {

    Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    DCartService userServiceDCartService;
    /**
     * <p >
     * 功能：根据用户id获取会员名称
     * </p>
     * @param
     * @author wuyubin
     * @date  2020年04月23日
     * @return
     */
    public String getNameById(Long userId) {
        return "admin";
    }
    /**
     * <p >
     * 功能：根据用户id查询用户信息
     * </p>
     * @param
     * @author wuyubin
     * @date  2020年04月23日
     * @return
     */
    public DUserDTO getByUserId(Long userId) {
        DUserDTO dto = null;
        String userName = getNameById(userId);
        if (null != userName && !"".equals(userName)) {
            dto = new DUserDTO();
            dto.setUserId(userId);
            dto.setUserName(userName);
            dto.setCreateTime(new Date());
        }
        LOGGER.info("i am old project dubbo method:getByUserId,userId:{}",userId);
        return dto;
    }
    /**
     * <p >
     * 功能：添加购物车（仅仅演示SpringCloud和Dubbo集成）
     * </p>
     * @param
     * @author wuyubin
     * @date  2020年04月23日
     * @return
     */
    public String addCart(Long userId,Long goodsId) {
        return userServiceDCartService.addCart(userId,goodsId);
    }

}
