package com.chinawu.cloud.cart.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.annotation.Service;
import com.chinawu.cloud.cart.interfaces.DCartService;
import com.chinawu.old.user.facade.dto.DUserDTO;
import com.chinawu.old.user.facade.interfaces.DUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p class="detail">
 * 功能：购物车服务类
 * </p>
 *
 * @ClassName: DCartServiceImpl
 * @version V1.0
 * @author wuyubin
 * @date 2020年04月22日 16:36
 */
@Service
public class DCartServiceImpl implements DCartService {

    Logger LOGGER = LoggerFactory.getLogger(DCartServiceImpl.class);

    @Reference(check = false)
    DUserService dUserService;
    /**
     * <p >
     * 功能：加入购物车
     * </p>
     * @param userId 用户id
     * @param goodsId 物品id
     * @author wuyubin
     * @date  2020年04月22日
     * @return
     */
    public String addCart(Long userId,Long goodsId) {
        DUserDTO userDTO = dUserService.getByUserId(userId);
        LOGGER.info("i am springcloud project dubbo method:addCart,userId:{},goodsId:{}",userId,goodsId);
        return userDTO.toString()+" addCart goodsId:"+goodsId+" success";
    }

}
