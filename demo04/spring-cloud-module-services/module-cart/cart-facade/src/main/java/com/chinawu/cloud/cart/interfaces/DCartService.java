package com.chinawu.cloud.cart.interfaces;

/**
 * <p class="detail">
 * 功能：购物车对外Dubbo接口定义类
 * </p>
 *
 * @ClassName: DCartService
 * @version V1.0
 * @author wuyubin
 * @date 2020年04月22日 16:30
 */
public interface DCartService {

    /**
     * <p >
     * 功能：加入购物车
     * </p>
     * @param userId 用户id
     * @param goodsId 物品id
     * @author wuyubin
     * @date  2020年04月22日
     * @return
     */
    String addCart(Long userId,Long goodsId);

}
