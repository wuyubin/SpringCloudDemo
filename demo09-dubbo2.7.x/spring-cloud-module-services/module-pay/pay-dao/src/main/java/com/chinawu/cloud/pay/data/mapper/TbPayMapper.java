package com.chinawu.cloud.pay.data.mapper;

import com.chinawu.cloud.pay.data.bean.TbPay;

public interface TbPayMapper {
    int deleteByPrimaryKey(Long id);

    int insert(TbPay record);

    int insertSelective(TbPay record);

    TbPay selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(TbPay record);

    int updateByPrimaryKey(TbPay record);
}