package com.chinawu.cloud.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p class="detail">
 * 功能：用户服务启动应用类
 * </p>
 *
 * @ClassName: CartServiceApplication
 * @version V1.0
 * @author wuyubin
 * @date 2020年04月22日 17:00
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAutoConfiguration
public class UserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }

}
