package com.chinawu.cloud.system.controller;

import com.chinawu.cloud.system.integration.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p class="detail">
 * 功能：系统服务模块接口访问类
 * </p>
 *
 * @author wuyubin
 * @version V1.0
 * @ClassName: SystemController
 * @date 2020年03月28日 16:35
 * Copyright 2020 chinawu.com, Inc. All rights reserved
 */
@RestController
@RequestMapping(value = "/web/system")
public class SystemController {

    @Autowired
    ConfigService configService;

    @RequestMapping(value = "/getEnvName", method = RequestMethod.GET)
    public String getEnvName() {
        return configService.getEnvName();
    }

}
