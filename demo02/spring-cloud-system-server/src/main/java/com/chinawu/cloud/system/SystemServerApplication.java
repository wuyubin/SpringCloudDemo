package com.chinawu.cloud.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p class="detail">
 * 功能：系统服务模块启动类
 * </p>
 *
 * @author wuyubin
 * @version V1.0
 * @ClassName: SystemServerApplication
 * @date 2020年03月28日 16:35
 * Copyright 2020 chinawu.com, Inc. All rights reserved
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class SystemServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SystemServerApplication.class, args);
    }

}
