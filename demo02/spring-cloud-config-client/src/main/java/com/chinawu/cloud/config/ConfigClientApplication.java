package com.chinawu.cloud.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * <p class="detail">
 * 功能：SpringCloud Config客户端
 * </p>
 *
 * @author wuyubin
 * @version V1.0
 * @ClassName: ConfigClientApplication
 * @date 2020年03月22日 18:35
 * Copyright 2020 chinawu.com, Inc. All rights reserved
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ConfigClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConfigClientApplication.class, args);
    }

}
