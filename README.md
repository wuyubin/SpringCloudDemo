# SpringCloudDemo

#### 介绍
SpringCloud 学习练手demo

#### 目录说明
demo01:SpringCloud系列之配置中心（Config）使用说明

demo02:SpringCloud系列之服务注册发现（Eureka）应用篇

demo03:SpringCloud系列之网关（Gateway）应用篇

demo04:SpringCloud系列之集成Dubbo应用篇 （dubbo 2.6.5）

demo04-dubbo2.7.6:SpringCloud系列之集成Dubbo应用篇（dubbo 2.7.6）

demo05:SpringCloud系列之集成分布式事务Seata应用篇

demo06:待完善

demo07:SpringCloud系列之Nacos应用篇

demo08:SpringCloud系列之Nacos+Dubbo应用篇（dubbo 2.6.x）

demo08-dubbo2.7.x:SpringCloud系列之Nacos+Dubbo应用篇（dubbo 2.7.x）

demo09:SpringCloud系列之Nacos+Dubbo+Seata应用篇（dubbo 2.6.x）

demo09-dubbo2.7.x:SpringCloud系列之Nacos+Dubbo+Seata应用篇（dubbo 2.7.x）

demo10:分库分表ShardingSphere应用篇

#### 说明
个人技术微信公众号，感兴趣的可以关注下哦，回复关键字**666**获取开发常用工具包
![输入图片说明](https://images.gitee.com/uploads/images/2020/0328/200612_237dfb44_72111.jpeg "qrcode15.jpg")

