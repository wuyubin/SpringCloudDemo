package com.chinawu.cloud.gateway.config;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import reactor.core.publisher.Mono;

@Configuration
public class RequestRateLimiterConfig {
    @Bean
    @Primary
    public KeyResolver apiKeyResolver() {
        //URL限流,超出限流返回429状态
        return exchange -> Mono.just(exchange.getRequest().getPath().toString());
    }
}
