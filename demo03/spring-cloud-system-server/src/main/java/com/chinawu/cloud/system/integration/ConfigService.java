package com.chinawu.cloud.system.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * <p class="detail">
 * 功能：系统服务集成类
 * </p>
 *
 * @author wuyubin
 * @version V1.0
 * @ClassName: ConfigService
 * @date 2020年03月28日 16:35
 * Copyright 2020 chinawu.com, Inc. All rights reserved
 */
@FeignClient(name = "config-client")
public interface ConfigService {

    @GetMapping(path = "/getEnv")
    String getEnvName();

}
