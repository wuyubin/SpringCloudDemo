package com.chinawu.cloud.config.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <p class="detail">
 * 功能：SpringCloud Config客户端Web接口访问类
 * </p>
 *
 * @author wuyubin
 * @version V1.0
 * @ClassName: ConfigClientWebController
 * @date 2020年03月22日 18:35
 * Copyright 2020 chinawu.com, Inc. All rights reserved
 */
@RestController
public class ConfigClientWebController {

    @Value("${app.env.name}")
    private String appEnvName;

    @RequestMapping("/getEnv")
    public String getEnv() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())+",envName:"+appEnvName;
    }
}
