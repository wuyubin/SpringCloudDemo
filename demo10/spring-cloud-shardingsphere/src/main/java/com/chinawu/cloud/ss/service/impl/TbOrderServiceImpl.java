package com.chinawu.cloud.ss.service.impl;

import com.chinawu.cloud.ss.mybatis.mapper.TbOrderMapper;
import com.chinawu.cloud.ss.mybatis.pojo.TbOrder;
import com.chinawu.cloud.ss.service.TbOrderService;
import com.chinawu.cloud.ss.service.TbShardingConfigService;
import com.chinawu.cloud.ss.util.ShardingIdUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;

/**
 * <p class="detail">
 * 功能：复合分片服务接口定义实现类
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: TbOrderServiceImpl
 * @date 2021年07月28日 10:03
 */
@Service("tbOrderServiceImpl")
public class TbOrderServiceImpl implements TbOrderService {

    @Autowired
    TbOrderMapper tbOrderMapper;
    @Autowired
    TbShardingConfigService tbShardingConfigServiceImpl;

    /**
     * <p >
     * 功能：新增
     * </p>
     * @param
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    public String addRecord() {
        TbOrder record = new TbOrder();
        String shopId = "100";
        String tradeNo = ShardingIdUtils.getShardingId(tbShardingConfigServiceImpl.getDbTbByShopId(shopId));
        record.setId(ShardingIdUtils.getShardingId(tbShardingConfigServiceImpl.getDbTbByShopId(shopId)));
        record.setShopId(Long.parseLong(shopId));
        record.setMoney(new BigDecimal("199"));
        record.setOrderNo(tradeNo);
        record.setCreateTime(System.currentTimeMillis());
        record.setUpdateTime(record.getCreateTime());
        record.setIsDeleted(Byte.valueOf("0"));
        tbOrderMapper.insert(record);
        return "success";
    }
    /**
     * <p >
     * 功能：根据店铺id查询记录
     * </p>
     * @param shopId 店铺id
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    public TbOrder selectByShopId(String shopId) {
        return tbOrderMapper.selectByShopId(shopId);
    }
    /**
     * <p >
     * 功能：根据订单id查询记录
     * </p>
     * @param id 订单id
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    public TbOrder selectById(String id) {
        return tbOrderMapper.selectByPrimaryKey(id);
    }

}
