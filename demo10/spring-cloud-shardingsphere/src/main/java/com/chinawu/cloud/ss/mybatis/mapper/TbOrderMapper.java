package com.chinawu.cloud.ss.mybatis.mapper;

import com.chinawu.cloud.ss.mybatis.pojo.TbOrder;
import org.apache.ibatis.annotations.Param;

public interface TbOrderMapper {

    int insert(TbOrder record);

    TbOrder selectByPrimaryKey(String id);

    TbOrder selectByShopId(@Param("shopId") String shopId);

}