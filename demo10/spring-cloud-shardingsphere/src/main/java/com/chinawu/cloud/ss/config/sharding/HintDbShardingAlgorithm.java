package com.chinawu.cloud.ss.config.sharding;

import org.apache.shardingsphere.api.sharding.hint.HintShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.hint.HintShardingValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * <p class="detail">
 * 功能：强制路由 hint 数据源算法
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: HintTbShardingAlgorithm
 * @date 2021年07月28日 16:33
 */
public class HintDbShardingAlgorithm implements HintShardingAlgorithm {

    @Override
    public Collection<String> doSharding(Collection collection, HintShardingValue hintShardingValue) {
        System.out.println("[Hint算法]数据源");
        // 获取shardingshpere配置文件中所配置的数据源
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println("配置数据源:"+iterator.next().toString());
        }
        // 强制路由数据源代码指定参数
        collection = hintShardingValue.getValues();
        iterator = collection.iterator();
        String dataSource = "";
        while (iterator.hasNext()) {
            dataSource = iterator.next().toString();
        }
        List<String> shardingSuffix = new ArrayList<>();
        // 实际数据源
        shardingSuffix.add(dataSource);
        System.out.println("[Hint算法]数据源:"+dataSource);
        return shardingSuffix;
    }




}
