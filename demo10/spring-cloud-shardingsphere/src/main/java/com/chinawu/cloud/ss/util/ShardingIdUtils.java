package com.chinawu.cloud.ss.util;

import java.text.SimpleDateFormat;
import java.util.UUID;

/**
 * <p class="detail">
 * 功能：复合分片分片键id生成工具
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: ShardingIdUtils
 * @date 2021年07月28日 16:39
 */
public class ShardingIdUtils {

    /**
     * <p class="detail">
     * 功能：获取订单27位数字编号
     * 分库(1位)+分表(2位)+yyyyMMddHHmmss(14位)+UUID(10位)
     * </p>
     * @param dbTb 分库分表
     * @return
     */
    public static String getShardingId(String dbTb) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return dbTb+sdf.format(System.currentTimeMillis()) + getStringIdByUUId();
    }


    private static String getStringIdByUUId() {
        int hashCodeV = UUID.randomUUID().toString().hashCode();
        if (hashCodeV < 0) {// 有可能是负数
            hashCodeV = -hashCodeV;
        }
        // 0 代表前面补充0
        // 4 代表长度为4
        // d 代表参数为正数型
        return String.format("%010d", hashCodeV);
    }

}
