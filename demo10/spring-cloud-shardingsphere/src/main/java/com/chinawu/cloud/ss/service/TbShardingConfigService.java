package com.chinawu.cloud.ss.service;

import com.chinawu.cloud.ss.mybatis.pojo.TbShardingConfig;

/**
 * <p class="detail">
 * 功能：复合分片自定义分片服务接口定义类
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: TbShardingConfigService
 * @date 2021年07月28日 16:53
 */
public interface TbShardingConfigService {

    /**
     * 根据店铺id获取分库信息
     * @param shopId
     * @return
     */
    String getDbByShopId(String shopId);
    /**
     * 根据店铺id获取分表信息
     * @param shopId
     * @return
     */
    String getTbByShopId(String shopId);
    /**
     * 根据店铺id获取分库分表信息
     * @param shopId
     * @return
     */
    String getDbTbByShopId(String shopId);

    /**
     * 根据业务id及业务类型查询分库分表信息
     * @param businessId
     * @param businessType
     * @return
     */
    TbShardingConfig getByBusinessIdAndBusinessType(String businessId, String businessType);

}
