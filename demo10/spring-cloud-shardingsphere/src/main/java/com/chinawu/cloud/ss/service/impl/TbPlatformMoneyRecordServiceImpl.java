package com.chinawu.cloud.ss.service.impl;

import com.chinawu.cloud.ss.mybatis.mapper.TbPlatformMoneyRecordMapper;
import com.chinawu.cloud.ss.mybatis.pojo.TbPlatformMoneyRecord;
import com.chinawu.cloud.ss.service.TbPlatformMoneyRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p class="detail">
 * 功能：精确分片服务接口定义实现类
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: TbPlatformMoneyRecordServiceImpl
 * @date 2021年07月28日 11:50
 */
@Service
public class TbPlatformMoneyRecordServiceImpl implements TbPlatformMoneyRecordService {

    @Autowired
    TbPlatformMoneyRecordMapper tbPlatformMoneyRecordMapper;
    /**
     * <p >
     * 功能：新增
     * </p>
     * @param
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    @Override
    public String addRecord() {
        TbPlatformMoneyRecord record = new TbPlatformMoneyRecord();
        record.setBusinessType(Byte.valueOf("10"));
        record.setBusinessId(System.currentTimeMillis()+"");
        record.setMoney(new BigDecimal("100"));
        record.setNote("test");
        record.setCreateTime(System.currentTimeMillis());
        record.setUpdateTime(record.getCreateTime());
        record.setIsDeleted(Byte.valueOf("0"));
        tbPlatformMoneyRecordMapper.insert(record);
        return "success";
    }
    /**
     * <p >
     * 功能：根据时间查询 =
     * </p>
     * @param time 时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    public TbPlatformMoneyRecord selectByTime(Long time) {
        return tbPlatformMoneyRecordMapper.selectByTime(time);
    }
    /**
     * <p >
     * 功能：根据时间查询 in
     * </p>
     * @param times 时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    public List<TbPlatformMoneyRecord> selectByTimes(List<Long> times) {
        return tbPlatformMoneyRecordMapper.selectByTimes(times);
    }
    /**
     * <p >
     * 功能：根据时间查询 between and
     * </p>
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    public List<TbPlatformMoneyRecord> selectByList(Long startTime, Long endTime) {
        return tbPlatformMoneyRecordMapper.selectByList(startTime,endTime);
    }
    /**
     * <p >
     * 功能：根据时间查询 >=
     * </p>
     * @param startTime 开始时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    public List<TbPlatformMoneyRecord> selectByGreaterthanTime(Long startTime) {
        return tbPlatformMoneyRecordMapper.selectByGreaterthanTime(startTime);
    }
    /**
     * <p >
     * 功能：根据时间查询 <=
     * </p>
     * @param endTime 结束时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    public List<TbPlatformMoneyRecord> selectByLessthanTime(Long endTime) {
        return tbPlatformMoneyRecordMapper.selectByLessthanTime(endTime);
    }
}
