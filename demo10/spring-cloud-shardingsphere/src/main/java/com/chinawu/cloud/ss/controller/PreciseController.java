package com.chinawu.cloud.ss.controller;

import com.alibaba.fastjson.JSON;
import com.chinawu.cloud.ss.service.TbPlatformMoneyRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * <p class="detail">
 * 功能：单分片键
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: PreciseController
 * @date 2021年07月28日 17:08
 */
@RestController
@RequestMapping("/precise/record")
public class PreciseController {

    @Autowired
    TbPlatformMoneyRecordService tbPlatformMoneyRecordServiceImpl;

    /**
     * <p >
     * 功能：新增 精确分片 =
     * </p>
     * @param 
     * @author wuyubin
     * @date 2021年07月28日
     * @return 
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add() {
        return tbPlatformMoneyRecordServiceImpl.addRecord();
    }
    /**
     * <p >
     * 功能：查询 精确分片 =
     * </p>
     * @param
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    @RequestMapping(value = "/selectByTime", method = RequestMethod.GET)
    public String selectByTime(Long time) {
        return JSON.toJSONString(tbPlatformMoneyRecordServiceImpl.selectByTime(time));
    }
    /**
     * <p >
     * 功能：查询 精确分片 in
     * </p>
     * @param
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    @RequestMapping(value = "/selectByTimes", method = RequestMethod.GET)
    public String selectByTimes(String times) {
        String[] timesArr = times.split(",");
        List<Long> t = new ArrayList<>();
        for (String time : timesArr) {
            t.add(Long.valueOf(time));
        }
        return JSON.toJSONString(tbPlatformMoneyRecordServiceImpl.selectByTimes(t));
    }
    /**
     * <p >
     * 功能：查询 范围分片 between and
     * </p>
     * @param
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    @RequestMapping(value = "/selectByList", method = RequestMethod.GET)
    public String selectByList(Long startTime, Long endTime) {
        return JSON.toJSONString(tbPlatformMoneyRecordServiceImpl.selectByList(startTime,endTime));
    }
    /**
     * <p >
     * 功能：查询 范围分片 >=
     * </p>
     * @param
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    @RequestMapping(value = "/selectByGreaterthanTime", method = RequestMethod.GET)
    public String selectByTimeGreaterthan(Long startTime) {
        return JSON.toJSONString(tbPlatformMoneyRecordServiceImpl.selectByGreaterthanTime(startTime));
    }
    /**
     * <p >
     * 功能：查询 范围分片 <=
     * </p>
     * @param
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    @RequestMapping(value = "/selectByLessthanTime", method = RequestMethod.GET)
    public String selectByLessthanTime(Long endTime) {
        return JSON.toJSONString(tbPlatformMoneyRecordServiceImpl.selectByLessthanTime(endTime));
    }
}
