package com.chinawu.cloud.ss.mybatis.mapper;

import com.chinawu.cloud.ss.mybatis.pojo.TbPlatformMoneyRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TbPlatformMoneyRecordMapper {
    int insert(TbPlatformMoneyRecord record);

    TbPlatformMoneyRecord selectByTime(@Param("time") Long time);

    List<TbPlatformMoneyRecord> selectByTimes(@Param("times") List<Long> times);

    List<TbPlatformMoneyRecord> selectByList(@Param("startTime") Long startTime, @Param("endTime") Long endTime);

    List<TbPlatformMoneyRecord> selectByGreaterthanTime(@Param("startTime") Long startTime);

    List<TbPlatformMoneyRecord> selectByLessthanTime(@Param("endTime") Long endTime);


}