package com.chinawu.cloud.ss.service;

import com.chinawu.cloud.ss.mybatis.pojo.TbOrder;

/**
 * <p class="detail">
 * 功能：复合分片服务接口定义类
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: TbOrderService
 * @date 2021年07月28日 10:03
 */
public interface TbOrderService {

    /**
     * <p >
     * 功能：新增
     * </p>
     * @param 
     * @author wuyubin
     * @date 2021年07月28日
     * @return 
     */
    String addRecord();
    /**
     * <p >
     * 功能：根据店铺id查询记录
     * </p>
     * @param shopId 店铺id
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    TbOrder selectByShopId(String shopId);
    /**
     * <p >
     * 功能：根据订单id查询记录
     * </p>
     * @param id 订单id
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    TbOrder selectById(String id);
}
