package com.chinawu.cloud.ss.config.sharding;

import org.apache.shardingsphere.api.sharding.hint.HintShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.hint.HintShardingValue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * <p class="detail">
 * 功能：强制路由 hint 数据表算法
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: HintTbShardingAlgorithm
 * @date 2021年07月28日 16:33
 */
public class HintTbShardingAlgorithm implements HintShardingAlgorithm {

    @Override
    public Collection<String> doSharding(Collection collection, HintShardingValue hintShardingValue) {
        System.out.println("[Hint算法]数据表");
        // 逻辑表
        String logicTableName = hintShardingValue.getLogicTableName();
        String tableName = "";
        // 获取shardingshpere配置文件中所配置的数据表
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println("配置数据表:"+iterator.next().toString());
        }
        // 强制路由数据源代码指定参数
        collection = hintShardingValue.getValues();
        iterator = collection.iterator();
        while (iterator.hasNext()) {
            tableName = iterator.next().toString();
        }
        List<String> shardingSuffix = new ArrayList<>();
        // 实际数据表
        shardingSuffix.add(tableName);
        System.out.println("[Hint算法]数据表:"+tableName);
        return shardingSuffix;
    }
}
