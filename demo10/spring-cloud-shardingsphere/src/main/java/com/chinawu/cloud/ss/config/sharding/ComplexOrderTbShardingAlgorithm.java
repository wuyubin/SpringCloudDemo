package com.chinawu.cloud.ss.config.sharding;

import com.chinawu.cloud.ss.mybatis.pojo.TbShardingConfig;
import com.chinawu.cloud.ss.service.TbShardingConfigService;
import org.apache.shardingsphere.api.sharding.complex.ComplexKeysShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.complex.ComplexKeysShardingValue;
import java.util.*;

/**
 * <p class="detail">
 * 功能：复合分片键数据表算法
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: ComplexOrderTbShardingAlgorithm
 * @date 2021年07月28日 13:45
 */

public class ComplexOrderTbShardingAlgorithm implements ComplexKeysShardingAlgorithm {

    public ComplexOrderTbShardingAlgorithm(){}


    private TbShardingConfigService tbShardingConfigServiceImpl;

    public void init(TbShardingConfigService dbTbShardingServerImpl){
        this.tbShardingConfigServiceImpl = dbTbShardingServerImpl;
    }

    @Override
    public Collection<String> doSharding(Collection collection, ComplexKeysShardingValue complexKeysShardingValue) {
        System.out.println("[多分片键复合分片算法]数据表");
        String tableName = complexKeysShardingValue.getLogicTableName();
        Map<String, Collection> shardingKeys = complexKeysShardingValue.getColumnNameAndShardingValuesMap();

        // id分片键
        Collection<String> idValues = shardingKeys.get("id");
        // shop_id分片键
        Collection<String> shopIdValues = shardingKeys.get("shop_id");
        // order_no分片键
        Collection<String> orderNoValues = shardingKeys.get("order_no");
        // id分片键值
        System.out.println("idValues:"+idValues);
        // shop_id分片键值
        System.out.println("shopIdValues:"+shopIdValues);
        // order_no分片键值
        System.out.println("orderNoValues:"+orderNoValues);

        // 获取shardingshpere配置文件中所配置的数据表
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println("配置数据表:"+iterator.next().toString());
        }
        // id 长度为27位, 分库(1位)+分表(2位)+yyyyMMddHHmmss(14位)+UUID(10位)
        if (null != idValues) {
            for (String value : idValues) {
                if(value.length() == 27){
                    tableName = tableName+"_"+value.substring(1, 3);
                }
            }
        } else if (null != shopIdValues) {
            // shop_id
            for (String value : shopIdValues) {
                TbShardingConfig dbtbSharding = tbShardingConfigServiceImpl.getByBusinessIdAndBusinessType(value,"SHOPID");
                tableName = tableName+"_"+dbtbSharding.getTbSharding();
            }
        } else if (null != orderNoValues) {
            // trade_no
            for (String value : orderNoValues) {
                if(value.length() == 27){
                    tableName = tableName+"_"+value.substring(1, 3);
                }
            }
        }
        List<String> shardingSuffix = new ArrayList<>();
        System.out.println("[多分片键复合分片算法]数据表:"+tableName);
        shardingSuffix.add(tableName);
        return shardingSuffix;
    }
}
