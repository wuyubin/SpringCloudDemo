package com.chinawu.cloud.ss.service.impl;

import com.chinawu.cloud.ss.mybatis.mapper.TbShardingConfigMapper;
import com.chinawu.cloud.ss.mybatis.pojo.TbShardingConfig;
import com.chinawu.cloud.ss.service.TbShardingConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p class="detail">
 * 功能：复合分片自定义分片服务接口定义实现类
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: TbShardingConfigServiceImpl
 * @date 2021年07月28日 16:54
 */
@Service("tbShardingConfigServiceImpl")
public class TbShardingConfigServiceImpl implements TbShardingConfigService {

    @Autowired
    TbShardingConfigMapper tbShardingConfigMapper;

    /**
     * 根据店铺id获取分库信息
     * @param shopId
     * @return
     */
    public String getDbByShopId(String shopId) {
        TbShardingConfig config = selectByBusinessIdAndBusinessType(shopId,"SHOPID");
        if (config != null) {
            return config.getDbSharding();
        }
        return null;
    }

    /**
     * 根据店铺id获取分表信息
     * @param shopId
     * @return
     */
    public String getTbByShopId(String shopId) {
        TbShardingConfig config = selectByBusinessIdAndBusinessType(shopId,"SHOPID");
        if (config != null) {
            return config.getTbSharding();
        }
        return null;
    }
    /**
     * 根据店铺id获取分库分表信息
     * @param shopId
     * @return
     */
    public String getDbTbByShopId(String shopId) {
        TbShardingConfig config = selectByBusinessIdAndBusinessType(shopId,"SHOPID");
        if (config != null) {
            return config.getDbSharding()+config.getTbSharding();
        }
        return null;
    }
    /**
     * 根据业务id及业务类型查询分库分表信息
     * @param businessId
     * @param businessType
     * @return
     */
    public TbShardingConfig getByBusinessIdAndBusinessType(String businessId, String businessType) {
        return selectByBusinessIdAndBusinessType(businessId,businessType);
    }

    private TbShardingConfig selectByBusinessIdAndBusinessType(String businessId, String businessType) {
        return tbShardingConfigMapper.selectByBusinessIdAndBusinessType(businessId,businessType);
    }
}

