package com.chinawu.cloud.ss.mybatis.pojo;

public class TbShardingConfig {
    private String id;

    private String businessType;

    private String businessId;

    private String dbSharding;

    private String tbSharding;

    private Long addTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType == null ? null : businessType.trim();
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId == null ? null : businessId.trim();
    }

    public String getDbSharding() {
        return dbSharding;
    }

    public void setDbSharding(String dbSharding) {
        this.dbSharding = dbSharding == null ? null : dbSharding.trim();
    }

    public String getTbSharding() {
        return tbSharding;
    }

    public void setTbSharding(String tbSharding) {
        this.tbSharding = tbSharding == null ? null : tbSharding.trim();
    }

    public Long getAddTime() {
        return addTime;
    }

    public void setAddTime(Long addTime) {
        this.addTime = addTime;
    }
}