package com.chinawu.cloud.ss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p class="detail">
 * 功能：这里写类描述
 * </p>
 *
 * @author wuyubin
 * @version V1.0
 * @ClassName: MainController
 * @date 2021年07月28日 16:51
 */
@SpringBootApplication
public class MainController {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(MainController.class, args);
    }

}
