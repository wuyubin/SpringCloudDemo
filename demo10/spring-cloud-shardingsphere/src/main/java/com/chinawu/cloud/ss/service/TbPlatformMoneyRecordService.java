package com.chinawu.cloud.ss.service;

import com.chinawu.cloud.ss.mybatis.pojo.TbPlatformMoneyRecord;

import java.util.List;

/**
 * <p class="detail">
 * 功能：精确分片服务接口定义类
 * </p>
 *
 * @author wuyubin
 * @version V1.0
 * @ClassName: TbPlatformMoneyRecordService
 * @date 2021年07月28日 11:50
 */
public interface TbPlatformMoneyRecordService {

    /**
     * <p >
     * 功能：新增
     * </p>
     * @param 
     * @author wuyubin
     * @date 2021年07月28日
     * @return 
     */
    String addRecord();
    /**
     * <p >
     * 功能：根据时间查询 =
     * </p>
     * @param time 时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    TbPlatformMoneyRecord selectByTime(Long time);
    /**
     * <p >
     * 功能：根据时间查询 in
     * </p>
     * @param times 时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    List<TbPlatformMoneyRecord> selectByTimes(List<Long> times);
    /**
     * <p >
     * 功能：根据时间查询 between and
     * </p>
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    List<TbPlatformMoneyRecord> selectByList(Long startTime, Long endTime);
    /**
     * <p >
     * 功能：根据时间查询 >=
     * </p>
     * @param startTime 开始时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    List<TbPlatformMoneyRecord> selectByGreaterthanTime(Long startTime);
    /**
     * <p >
     * 功能：根据时间查询 <=
     * </p>
     * @param endTime 结束时间
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    List<TbPlatformMoneyRecord> selectByLessthanTime(Long endTime);
}
