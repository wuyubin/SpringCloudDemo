package com.chinawu.cloud.ss.controller;

import com.alibaba.fastjson.JSON;
import com.chinawu.cloud.ss.service.impl.TbOrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p class="detail">
 * 功能：复合分片控制类
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: ComplexController
 * @date 2021年07月28日 17:08
 */
@RestController
@RequestMapping("/complex/record")
public class ComplexController {

    @Autowired
    TbOrderServiceImpl tbOrderServiceImpl;

    /**
     * <p >
     * 功能：新增
     * </p>
     * @param 
     * @author wuyubin
     * @date 2021年07月28日
     * @return 
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add() {
        return tbOrderServiceImpl.addRecord();
    }
    /**
     * <p >
     * 功能：根据shopId查询
     * </p>
     * @param shopId
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    @RequestMapping(value = "/selectByShopId", method = RequestMethod.GET)
    public String selectByShopId(String shopId) {
        return JSON.toJSONString(tbOrderServiceImpl.selectByShopId(shopId));
    }
    /**
     * <p >
     * 功能：根据id查询
     * </p>
     * @param id
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    @RequestMapping(value = "/selectById", method = RequestMethod.GET)
    public String selectById(String id) {
        return JSON.toJSONString(tbOrderServiceImpl.selectById(id));
    }
}
