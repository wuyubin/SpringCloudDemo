package com.chinawu.cloud.ss.controller;

import com.alibaba.fastjson.JSON;
import com.chinawu.cloud.ss.service.TbHintService;
import com.chinawu.cloud.ss.service.TbPlatformMoneyRecordService;
import org.apache.shardingsphere.api.hint.HintManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p class="detail">
 * 功能：强制路由控制类
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: HintController
 * @date 2021年07月28日 17:08
 */
@RestController
@RequestMapping("/hint/record")
public class HintController {

    @Autowired
    TbHintService tbHintServiceImpl;

    /**
     * <p >
     * 功能：新增
     * </p>
     * @param 
     * @author wuyubin
     * @date 2021年07月28日
     * @return 
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add() {
        return JSON.toJSONString(tbHintServiceImpl.addRecord());
    }
}
