package com.chinawu.cloud.ss.service.impl;

import com.chinawu.cloud.ss.mybatis.mapper.TbHintMapper;
import com.chinawu.cloud.ss.mybatis.pojo.TbHint;
import com.chinawu.cloud.ss.service.TbHintService;
import org.apache.shardingsphere.api.hint.HintManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p class="detail">
 * 功能：强制路由服务接口实现类
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: TbHintServiceImpl
 * @date 2021年07月28日 13:24
 */
@Service("tbHintServiceImpl")
public class TbHintServiceImpl implements TbHintService {

    @Autowired
    TbHintMapper tbHintMapper;

    /**
     * <p >
     * 功能：新增
     * </p>
     * @param
     * @author wuyubin
     * @date 2021年07月28日
     * @return
     */
    public TbHint addRecord() {
        HintManager hintManager = HintManager.getInstance();
        // 强制路由分库 对应 spring.shardingsphere.datasource.names=ds,ds-1,ds-2 设置
        hintManager.addDatabaseShardingValue("tb_hint", "ds-2");
        // 强制路由分表
        hintManager.addTableShardingValue("tb_hint", "tb_hint_11");

        TbHint record = new TbHint();
        record.setNote("测试数据");
        record.setCreateTime(System.currentTimeMillis());
        record.setUpdateTime(record.getCreateTime());
        record.setIsDeleted(Byte.valueOf("0"));
        tbHintMapper.insert(record);
        hintManager.close();
        return record;
    }
}
