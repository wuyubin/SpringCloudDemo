package com.chinawu.cloud.ss.config.sharding;

import com.chinawu.cloud.ss.mybatis.pojo.TbShardingConfig;
import com.chinawu.cloud.ss.service.TbShardingConfigService;
import org.apache.shardingsphere.api.sharding.complex.ComplexKeysShardingAlgorithm;
import org.apache.shardingsphere.api.sharding.complex.ComplexKeysShardingValue;
import java.util.*;

/**
 * <p class="detail">
 * 功能：复合分片键数据源算法
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: ComplexOrderDbShardingAlgorithm
 * @date 2021年07月28日 13:45
 */

public class ComplexOrderDbShardingAlgorithm implements ComplexKeysShardingAlgorithm {

    public ComplexOrderDbShardingAlgorithm(){}

    private TbShardingConfigService tbShardingConfigServiceImpl;

    public void init(TbShardingConfigService dbTbShardingServerImpl){
        this.tbShardingConfigServiceImpl = dbTbShardingServerImpl;
    }

    @Override
    public Collection<String> doSharding(Collection collection, ComplexKeysShardingValue complexKeysShardingValue) {
        System.out.println("[多分片键复合分片算法]数据源");
        // 默认数据源
        String dataSourcesName = "ds";
        Map<String, Collection> shardingKeys = complexKeysShardingValue.getColumnNameAndShardingValuesMap();

        // id分片键
        Collection<String> idValues = shardingKeys.get("id");
        // shop_id分片键
        Collection<String> shopIdValues = shardingKeys.get("shop_id");
        // order_no分片键
        Collection<String> orderNoValues = shardingKeys.get("order_no");
        // id分片键值
        System.out.println("idValues:"+idValues);
        // shop_id分片键值
        System.out.println("shopIdValues:"+shopIdValues);
        // order_no分片键值
        System.out.println("orderNoValues:"+orderNoValues);

        // 获取shardingshpere配置文件中所配置的数据源
        Iterator iterator = collection.iterator();
        while (iterator.hasNext()) {
            System.out.println("配置数据源:"+iterator.next().toString());
        }
        // id 长度为27位, 分库(1位)+分表(2位)+yyyyMMddHHmmss(14位)+UUID(10位)
        if (null != idValues) {
            for (String value : idValues) {
                if(value.length() == 27){
                    dataSourcesName = "ds-"+value.substring(0,1);
                }
            }
        } else if (null != shopIdValues) {
            // shop_id
            for (String value : shopIdValues) {
                TbShardingConfig dbtbSharding = tbShardingConfigServiceImpl.getByBusinessIdAndBusinessType(value,"SHOPID");
                dataSourcesName = "ds-"+dbtbSharding.getDbSharding();
            }
        } else if (null != orderNoValues) {
            // trade_no
            for (String value : orderNoValues) {
                if(value.length() == 27){
                    dataSourcesName = "ds-"+value.substring(0,1);
                }
            }
        }

        List<String> shardingSuffix = new ArrayList<>();
        System.out.println("[多分片键复合分片算法]数据源:"+dataSourcesName);
        shardingSuffix.add(dataSourcesName);
        return shardingSuffix;
    }
}
