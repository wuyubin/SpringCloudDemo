package com.chinawu.cloud.ss.mybatis.mapper;

import com.chinawu.cloud.ss.mybatis.pojo.TbHint;

public interface TbHintMapper {

    int insert(TbHint record);

}