package com.chinawu.cloud.ss.mybatis.mapper;

import com.chinawu.cloud.ss.mybatis.pojo.TbShardingConfig;
import org.apache.ibatis.annotations.Param;

public interface TbShardingConfigMapper {

    TbShardingConfig selectByBusinessIdAndBusinessType(@Param("businessId") String businessId,@Param("businessType") String businessType);
}