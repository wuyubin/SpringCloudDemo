package com.chinawu.cloud.ss.service;

import com.chinawu.cloud.ss.mybatis.pojo.TbHint;

import java.util.List;

/**
 * <p class="detail">
 * 功能：强制路由服务接口类
 * </p>
 *
 * @author wuyubin
 * Copyright 2021 chinawu.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: TbHintService
 * @date 2021年07月28日 13:24
 */
public interface TbHintService {

    /**
     * <p >
     * 功能：新增
     * </p>
     * @param 
     * @author wuyubin
     * @date 2021年07月28日
     * @return 
     */
    TbHint addRecord();

}
