package com.chinawu.cloud.user;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p class="detail">
 * 功能：用户控制类
 * </p>
 *
 * @author wuyubin
 * Copyright 2020 youx.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: ConfigController
 * @date 2020年08月19日 11:13
 */
@RestController
@RequestMapping("/user")
@RefreshScope
public class UserController {

    @Value("${spring.datasource.url}")
    private String datasourceUrl;

    @RequestMapping("/getDatasourceUrl")
    public String getDatasourceUrl() {
        return datasourceUrl;
    }
}