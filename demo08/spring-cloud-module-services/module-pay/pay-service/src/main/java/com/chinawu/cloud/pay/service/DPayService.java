package com.chinawu.cloud.pay.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.chinawu.cloud.pay.integration.DPayFacade;

/**
 * <p class="detail">
 * 功能：Dubbo服务提供者
 * </p>
 *
 * @author wuyubin
 * Copyright 2020 youx.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: DPayService
 * @date 2020年09月02日 17:17
 */
@Service
public class DPayService implements DPayFacade {

    @Override
    public String goToPay(String userName) {
        System.out.println("dubbo.method:goToPay request "+userName);
        return "dubbo.method:goToPay result:" + userName + " pay success";
    }
}
