package com.chinawu.cloud.pay;

import com.chinawu.cloud.user.integration.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p class="detail">
 * 功能：支付模块控制类
 * </p>
 *
 * @author wuyubin
 * Copyright 2020 youx.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: PayController
 * @date 2020年08月19日 11:13
 */
@RestController
@RequestMapping("/pay")
public class PayController {

    @Autowired
    UserFacade userFacade;

    @RequestMapping(value = "/get",method = RequestMethod.GET)
    public String get() {
        return userFacade.getByUserId(1l);
    }

}