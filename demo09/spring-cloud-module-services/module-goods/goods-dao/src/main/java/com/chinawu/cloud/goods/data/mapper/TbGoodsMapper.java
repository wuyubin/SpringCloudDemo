package com.chinawu.cloud.goods.data.mapper;

import com.chinawu.cloud.goods.data.bean.TbGoods;

public interface TbGoodsMapper {

    TbGoods selectById(Long id);

    int updateSubstractStockNumById(Long id);
}