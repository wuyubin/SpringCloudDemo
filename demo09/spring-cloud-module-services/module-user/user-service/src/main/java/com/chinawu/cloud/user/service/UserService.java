package com.chinawu.cloud.user.service;

import com.chinawu.cloud.user.data.bean.TbUser;
import com.chinawu.cloud.user.data.mapper.TbUserMapper;
import com.chinawu.cloud.user.integration.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p class="detail">
 * 功能：用户服务实现类
 * </p>
 *
 * @author wuyubin
 * Copyright 2020 youx.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: UserService
 * @date 2020年08月24日 16:28
 */
@RestController
public class UserService implements UserFacade {


    @Autowired
    TbUserMapper tbUserMapper;

    @Override
    public String getByUserId(Long userId) {
        System.out.println("feign.method:getByUserId request");
        TbUser user = tbUserMapper.selectByPrimaryKey(userId);
        return "feign.method:getByUserId result:"+user.getId()+":"+user.getName();
    }


}
