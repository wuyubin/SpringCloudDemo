package com.chinawu.cloud.pay.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.chinawu.cloud.pay.data.bean.TbPay;
import com.chinawu.cloud.pay.data.mapper.TbPayMapper;
import com.chinawu.cloud.pay.integration.DPayFacade;
import io.seata.core.context.RootContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

/**
 * <p class="detail">
 * 功能：Dubbo服务提供者
 * </p>
 *
 * @author wuyubin
 * Copyright 2020 youx.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: DPayService
 * @date 2020年09月02日 17:17
 */
@Service
public class DPayService implements DPayFacade {

    @Autowired
    TbPayMapper tbPayMapper;

    @Override
    public String goToPay(String userName) {
        String xid = RootContext.getXID();
        System.out.println("pay.seata.xid:"+xid);
        System.out.println("dubbo.method:goToPay request "+userName);
        TbPay pay = new TbPay();
        pay.setMoney(new BigDecimal("5"));
        pay.setStatus(Byte.valueOf("1"));
        tbPayMapper.insert(pay);
        return "dubbo.method:goToPay result:" + userName + " pay success";
    }
}
