package com.chinawu.cloud.order.data.mapper;

import com.chinawu.cloud.order.data.bean.TbOrder;

public interface TbOrderMapper {

    int insert(TbOrder record);

}