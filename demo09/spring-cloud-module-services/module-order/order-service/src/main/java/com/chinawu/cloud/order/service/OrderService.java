package com.chinawu.cloud.order.service;

import com.alibaba.dubbo.config.annotation.Reference;
import com.chinawu.cloud.cart.dto.CartDTO;
import com.chinawu.cloud.cart.integration.CartFacade;
import com.chinawu.cloud.goods.dto.GoodsDTO;
import com.chinawu.cloud.goods.integration.GoodsFacade;
import com.chinawu.cloud.order.data.bean.TbOrder;
import com.chinawu.cloud.order.data.mapper.TbOrderMapper;
import com.chinawu.cloud.order.integration.OrderFacade;
import com.chinawu.cloud.pay.integration.DPayFacade;
import com.chinawu.cloud.wallet.integration.WalletFacade;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RestController;

import java.beans.Transient;

/**
 * <p class="detail">
 * 功能：订单接口实现类
 * </p>
 *
 * @author wuyubin
 * Copyright 2020 youx.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: OrderService
 * @date 2020年05月22日 11:02
 */
@RestController
public class OrderService implements OrderFacade {

    @Autowired
    private TbOrderMapper tbOrderMapper;
    @Autowired
    private CartFacade cartFacade;
    @Autowired
    private GoodsFacade goodsFacade;
    @Autowired
    private WalletFacade walletFacade;

    // Dubbo服务消费
    @Reference(check = false)
    DPayFacade dPayFacade;

    /**
     * <p >
     * 功能：新增订单
     * </p>
     * @param cartId 购物车ID
     * @author wuyubin
     * @date  2020年05月22日
     * @return
     */
    @Override
    public void addOrder(Long cartId) {
        CartDTO cart = cartFacade.getCartById(cartId);
        TbOrder order = new TbOrder();
        order.setUserId(cart.getUserId());
        order.setGoodsId(cart.getGoodsId());
        order.setOrderNo(String.valueOf(System.currentTimeMillis()));
        order.setCreateTime(System.currentTimeMillis());
        order.setUpdateTime(order.getCreateTime());
        order.setIsDeleted(Byte.valueOf("0"));
        // 新增订单
        tbOrderMapper.insert(order);
        // 删除购物车(Feign)
        cartFacade.deleteCartById(cartId);
        GoodsDTO goods = goodsFacade.getByGoodsId(cart.getGoodsId());
        // 扣减库存(Feign)
        goodsFacade.substractStock(goods.getId());
        // 扣减金额(Feign)
        walletFacade.substractMoney(cart.getUserId(),goods.getMoney());
        // 记录支付消息（Dubbo调用）
        dPayFacade.goToPay("wuyubin");
        throw new RuntimeException();
    }
}
