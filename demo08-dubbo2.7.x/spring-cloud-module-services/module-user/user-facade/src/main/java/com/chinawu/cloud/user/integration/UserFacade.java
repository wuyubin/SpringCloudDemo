package com.chinawu.cloud.user.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <p class="detail">
 * 功能：用户模块Feign客户端
 * </p>
 *
 * @author wuyubin
 * Copyright 2020 youx.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: UserFacade
 * @date 2020年08月24日 16:23
 */
@FeignClient(name="user-service")
public interface UserFacade {

    @GetMapping(path="getByUserId")
    String getByUserId(@RequestParam("userId") Long userId);

}

