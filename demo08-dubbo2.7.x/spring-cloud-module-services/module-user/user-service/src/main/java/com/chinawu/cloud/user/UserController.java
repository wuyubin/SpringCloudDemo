package com.chinawu.cloud.user;

import com.chinawu.cloud.pay.integration.DPayFacade;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p class="detail">
 * 功能：用户控制类
 * </p>
 *
 * @author wuyubin
 * Copyright 2020 youx.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: ConfigController
 * @date 2020年08月19日 11:13
 */
@RestController
@RequestMapping("/user")
@RefreshScope
public class UserController {

    @Value("${spring.datasource.url}")
    private String datasourceUrl;
    // Dubbo服务消费
    @Reference(check = false)
    DPayFacade dPayFacade;

    /**
     * <p >
     * 功能：获取数据源连接配置信息
     * </p>
     * @param
     * @author wuyubin
     * @date  2020年9月3日
     * @return
     */
    @RequestMapping("/getDatasourceUrl")
    public String getDatasourceUrl() {
        return datasourceUrl;
    }

    /**
     * <p >
     * 功能：测试Dubbo服务调用
     * </p>
     * @param
     * @author wuyubin
     * @date  2020年9月3日
     * @return
     */
    @RequestMapping("/goToPay")
    public String goToPay() {
        return dPayFacade.goToPay("wuyubin");
    }
}