package com.chinawu.cloud.pay.integration;

/**
 * <p class="detail">
 * 功能：这里写类描述
 * </p>
 *
 * @author wuyubin
 * Copyright 2020 youx.com, Inc. All rights reserved
 * @version V1.0
 * @ClassName: DPayFacade
 * @date 2020年09月02日 17:16
 */
public interface DPayFacade {

    String goToPay(String userName);

}
