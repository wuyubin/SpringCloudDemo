package com.chinawu.cloud.pay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p class="detail">
 * 功能：支付服务启动应用类
 * </p>
 *
 * @ClassName: PayServiceApplication
 * @version V1.0
 * @author wuyubin
 * @date 2020年04月22日 17:00
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableAutoConfiguration
@EnableFeignClients(basePackages = {"com.chinawu.cloud.user.*"})
public class PayServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(PayServiceApplication.class, args);
    }

}
